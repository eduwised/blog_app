

#### Objective
In this assignment, You need to create an `Blog Application` where a user can post, comment, like, delete or edit based on roles. Front, backend and database can be of your choice.

#### Overview
- `Users` can be divided into two role like `admin` and `user`.
- `Users` can see a `list of posts` in there dashboard once they login. It will be a paginatead view with `10` posts per page.
- A `SignUp` page to take basic inputs like name, email, password and `role`(just for this project :) ).
- A `SignIn` page to redirect to users `dashboard` page (containing all posts) after authentication.
- A separate page to add a new `post` and
- A separate page to view each post through a `unique link`.

#### Roles
- **user**
	-	A user is a general user of the app who can post, like and comment
	- He/She should be shown options to `delete` or `edit` his/her own post

- **admin`**
	- `admin` will have access through out the length and breadth of the app
	- `admin` will have the following access
		- He/She will have all the same acess right as a normal user.
		- A separate page (with only `admin` acess) where he/she can take actions against a normal user like revoking a users access to the app.
		- Delete a post of a user

###### Other pages
- A profile page where a user can edit his/her own details . This page will be visible to all but only editable by the user.

###### Pointers
- Implementation of the backend and frontend logics
- Role based view for user and admin
- Efficiency of the app
- Hosting the app
- Proper documentation of the app along with how to install and run.

##### Submission
- Submit the app by uploading it to github or send a .zip file with proper docs
